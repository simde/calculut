# Conception

## Maquette

Nous avons réalisé une maquette Figma afin de concevoir l'interface et le fonctionnement de notre solution.

Elle est disponible ici : https://www.figma.com/proto/8F2NBWDmSAHcgZHb3o6VX5/diagramme-fast?type=design&node-id=117-612&t=svRpTYqmsV1pjZl0-1&scaling=scale-down&page-id=117%3A611&mode=design

Et aussi dans le dossier [documentation](/docs/) en pdf et fichier figma.

## Base de données

À la conception :
![](/docs/UML%20v2.drawio.png)

BDD développée :
![](/docs/bilan_formulaire.png)
